#define CATCH_CONFIG_RUNNER
#include "../Gateware-Development/UnitTests/catch.hpp"

#if defined(_WIN32) && !defined(NDEBUG) 
#include <crtdbg.h>
#include <stdlib.h>
#define _CRTDBG_MAO_ALLOC
#endif

#include <chrono>

// DO NOT REMOVE THIS, YOU CAN UNCOMMENT, BUT DONT REMOVEEEEE
#define DISABLE_USER_INPUT_TESTS 0


#define GATEWARE_ENABLE_CORE
#define GATEWARE_ENABLE_AUDIO
#define GATEWARE_ENABLE_MATH
#define GATEWARE_ENABLE_MATH2D
#define GATEWARE_ENABLE_SYSTEM
#define GATEWARE_ENABLE_GRAPHICS
#define GATEWARE_ENABLE_INPUT

#include "../Gateware-Development/build/Gateware.h"
#include "../Gateware-Development/UnitTests/tests.hpp"


int main(int argc, char** argv)
{
#if defined(_WIN32) && !defined(NDEBUG) 
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
	_CrtSetReportMode(_CRT_ERROR, _CRTDBG_MODE_DEBUG);
#endif

	auto startTime = std::chrono::system_clock::now();
	int result = Catch::Session().run(argc, argv);

	std::cout << "== EXITING TESTS ==\n";
	std::chrono::duration<float> time = std::chrono::system_clock::now() - startTime;
	printf("Elapsed Time: %fs\n", time.count());

	GW::SYSTEM::GFile fileProxy;
	fileProxy.Create();
	char workingDir[512] = "";
	fileProxy.GetSaveFolder(512, workingDir);
	if (result == 0) {
		fileProxy.SetCurrentWorkingDirectory(workingDir);
		if (+fileProxy.OpenTextWrite("UTResult.txt")) {
			fileProxy.Write((const char*)&result, sizeof(int));
			fileProxy.CloseFile();
		}
	}

	return result;
}
// this is ignored by desktop applications but allows UWP/iOS/Android to use the main entrypoint.
// without it you will need to define the specific platform entry point (ex: wWinMain in C++/WinRT)
GW::SYSTEM::GApp myApp(main);
