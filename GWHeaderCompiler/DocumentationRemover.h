#ifndef _DOCUMENTATION_REMOVER
#define _DOCUMENTATION_REMOVER
#include "pch.h"
#include "GWException.h"

//Right now, only doxygen is being removed. But eventually, all comments are going to be removed from the single header.
//When the compiler is fully refactored to remove all coments, this class should probably be removed or refactored to work on a single-file basis, rather than on the single header as a whole.
//Each file could have their comments removed *before* they're written to the single header
//If all comments are removed from the single header after it's compeltely written, the version infor and license will be removed.
//There's already a function that preprocesses the files to remove #includes and such (File::Preprocess). It makes sense to put the comment removal there. 
class DocumentationRemover
{
private:
	GW::SYSTEM::GFile fileHandler;
	bool writeIsOpen = false, readIsOpen = false;

	void OpenWrite();
	void OpenRead();
	void CloseFile();

public:
	DocumentationRemover() = default;
	DocumentationRemover(std::string _outputPath);

	void RemoveDoxygen();
};

#endif