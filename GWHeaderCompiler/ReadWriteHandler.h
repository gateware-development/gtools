#ifndef _READ_WRITE_HANDLER
#define _READ_WRITE_HANDLER
#include "FileHeap.h"
#include "pch.h"
#include <vector>

class ReadWriteHandler
{

public:
	ReadWriteHandler() = default;
	ReadWriteHandler(std::string _outputPath, GW::SYSTEM::GLog* _log);

private:
	GW::SYSTEM::GFile readHandler, writeHandler;
	GW::SYSTEM::GLog* log;

	void LogMessage(std::string _msg);
public:
	void OpenWrite();
	void CloseWrite();

	void WriteNewline();
	void WriteFile(const File* _toWrite);
	void WriteFiles(const std::vector<File*>* _filesToWrite);
	void WriteString(const std::string _toWrite);
	void WriteFileByPath(std::string _path);
	void WriteFilesByPath(std::vector<std::string>& _filePathsToWrite);

	void ReadFileIntoString(const std::string _filePath, std::string& _outString);

	void FlushFile();

	void WriteBeginMarker(const std::string& _name);
	void WriteEndMarker(const std::string& _name);

	void WriteBeginningOfIncludeGuard();
	void WriteEndOfIncludeGuard();
	void WriteCompilerVersionInfo();
	void WriteFileByPathAsComment(std::string _filePath);
};
#endif