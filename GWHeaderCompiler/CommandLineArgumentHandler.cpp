#include "CommandLineArgumentHandler.h"

CommandLineArgumentHandler::CommandLineArgumentHandler(int _argc, char** _argv)
{
	CopyArgumentsIntoVector(_argc, _argv);

	FindInterfacePath();
	FindOutputPath();
	FindLicensePath();
	FindHeaderListPath();
	FindPrependFilePaths();
	FindAppendFilePaths();
}

void CommandLineArgumentHandler::CopyArgumentsIntoVector(int _argc, char** _argv)
{
	for (size_t i = 1; i < _argc; i++) //the first command argument is the executable, and we don't care about him. Skip the first argument. 
	{
		commandLineArguments.push_back(_argv[i]);
	}
}

bool CommandLineArgumentHandler::ArgumentsAreValid()
{
	return HelpWasRequested() || ShouldGenerate();
}

bool CommandLineArgumentHandler::HelpWasRequested()
{
	return IndexOfArgument("--help") != -1;
}

bool CommandLineArgumentHandler::ShouldGenerate()
{
	return IndexOfArgument("--generate") != -1;
}

std::string CommandLineArgumentHandler::GetInterfacePath()
{
	return interfacePath;
}

std::string CommandLineArgumentHandler::GetOutputPath()
{
	return outputPath;
}

std::string CommandLineArgumentHandler::GetLicensePath()
{
	return licensePath;
}

std::string CommandLineArgumentHandler::GetHeaderListPath()
{
	return headerListPath;
}

std::vector<std::string> CommandLineArgumentHandler::GetPrependFilePaths()
{
	return prependFilePaths;
}
std::vector<std::string> CommandLineArgumentHandler::GetAppendFilePaths()
{
	return appendFilePaths;
}

void CommandLineArgumentHandler::PrintHelpInfo()
{
	std::cout << "Command line arguments (EVERYTHING IS CASE SENSITIVE)";
	std::cout << "\n\t[--help]: prints this help text";
	std::cout << "\n\t[--generate]: tells the compiler to generate the single header";
	std::cout << "\n\t[--interface]: path to interface folder, program will use to look for header files";
	std::cout << "\n\t[--output]: output directory, the directory where the generated header file will go to";
	std::cout << "\n\t[--license]: The license file. This file will get a \"/*\" prepended to it and a \"*/\" appended to it";
	std::cout << "\n\t[--prepend]: all arguments given after this one will be treated as filepaths to be prepended to the single header, until another command (denoted by a leading \"--\") is reached (put the version file here)";
	std::cout << "\n\t[--append]: all arguments given after this one will be treated as filepaths to be appended to the single header, until another command (denoted by a leading \"--\") is reached";
	std::cout << "\n\t[--header-list]: this should be the path to an external file that contains an ordered list of the header file names to be compiled, spearated by newlines. One header fiel per line, no commas, no quotation marks, no paths. Just the file names. They will be written to the single header in order, so make sure all of the dependencies are in the correct order!";
	std::cout << "\n\tAn example of correctly-configured arguments is as follows:";
	std::cout << "\n\t--generate --interface ./../../../Interface  --output ./ --license ../../../LICENSE --prepend ../../../Source/Shared/GVersion.hpp ./../../Source/Shared/WarningSuppressions.hpp  --append ./../../Source/Shared/WarningUnsuppressions.hpp --header-list ./../../DevOps/HeaderList.txt";
	std::cout << "\n";
}


void CommandLineArgumentHandler::FindInterfacePath()
{
	int indexOfInterfacePath = IndexOfArgument("--interface") + 1;
	if(indexOfInterfacePath == 0)
		throw std::runtime_error("No interface path specified!");

	interfacePath = commandLineArguments[indexOfInterfacePath];
}
void CommandLineArgumentHandler::FindOutputPath()
{
	int indexOfOutputPath = IndexOfArgument("--output") + 1;
	if (indexOfOutputPath == 0)
		throw std::runtime_error("No output path specified!");

	outputPath = commandLineArguments[indexOfOutputPath];
}

void CommandLineArgumentHandler::FindLicensePath()
{
	int indexOfLicensePath = IndexOfArgument("--license") + 1;
	if (indexOfLicensePath == 0)
		throw std::runtime_error("No license path specified!");

	licensePath = commandLineArguments[indexOfLicensePath];
}

void CommandLineArgumentHandler::FindHeaderListPath()
{
	int indexOfHeaderListPath = IndexOfArgument("--header-list") + 1;
	if (indexOfHeaderListPath == 0)
		throw std::runtime_error("No header list specified!");

	headerListPath = commandLineArguments[indexOfHeaderListPath];
}


void CommandLineArgumentHandler::FindPrependFilePaths()
{
	bool isPrependPath = false;

	//everything after "--prepend" and before a new command argument is a filepath to be prepended. 
	for (size_t i = 0; i < commandLineArguments.size(); i++)
	{
		if (commandLineArguments[i].find_first_of("--") != std::string::npos) //if we hit another command argument, 
			isPrependPath = false;

		if (isPrependPath)
			prependFilePaths.push_back(commandLineArguments[i]);

		if (commandLineArguments[i] == "--prepend")
			isPrependPath = true;
	}

}
void CommandLineArgumentHandler::FindAppendFilePaths()
{
	bool isAppendPath = false; //everything after "--append" and before a new command argument is a filepath to be appended. 

	for (size_t i = 0; i < commandLineArguments.size(); i++)
	{
		if (commandLineArguments[i].find_first_of("--") != std::string::npos) //if we hit another command argument, 
			isAppendPath = false;

		if (isAppendPath)
			appendFilePaths.push_back(commandLineArguments[i]);

		if (commandLineArguments[i] == "--append")
			isAppendPath = true;
	}
}

int CommandLineArgumentHandler::IndexOfArgument(std::string _argumentToFind)
{
	for (int i = 0; i < commandLineArguments.size(); i++)
	{
		if (commandLineArguments[i] == _argumentToFind)
			return i;
	}
	return -1;
}
