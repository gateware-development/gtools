#ifndef _PREDEFINEDREGEX_H_
#define _PREDEFINEDREGEX_H_
#include <regex>

namespace REGEX
{
	static const std::regex PoundInclude{ "#include \".*\"", std::regex_constants::ECMAScript | std::regex_constants::icase };

}
#endif