#include "FileSorter.h"
#include "GWException.h"

FileSorter::FileSorter(FileHeap* _headerHeap)
{
	headerHeap = _headerHeap;
}

std::vector<File*>* FileSorter::GetSortedHeaderList()
{
	if (headersAreSorted == false)
		SortHeaders();

	return &headerFiles;
}

void FileSorter::SetHeaderListPath(std::string _headerListPath)
{
	headerListPath = _headerListPath;
	headersAreSorted = false;
}

void FileSorter::ReadHeaderListFromFile()
{
	GW::SYSTEM::GFile reader;
	reader.Create();
	ThrowIfFailed(reader.OpenTextRead(headerListPath.data()));
	GW::GReturn retVal = GW::GReturn::SUCCESS;
	while (retVal == GW::GReturn::SUCCESS)
	{
		char curLine[256];
		retVal = reader.ReadLine(curLine, 256, '\n');
		std::string lineString;
		lineString.assign(curLine);
		if (lineString == "")
			break;
		desiredHeaderOrder.push_back(lineString);
	}
}

void FileSorter::SortHeaders()
{
	ReadHeaderListFromFile();
	headerFiles.clear();
	std::vector<File*> tempHeaderFilesList;
	CopyFileHeapToVector(headerHeap, tempHeaderFilesList);

	//get all of the specified files
	for (size_t i = 0; i < desiredHeaderOrder.size(); i++)
	{
		int index = GetFileIndexByName(desiredHeaderOrder[i], tempHeaderFilesList);
		if (index != -1)
		{
			headerFiles.push_back(tempHeaderFilesList[index]);
			tempHeaderFilesList[index] = nullptr;
		}
	}
	
	//get all of the unspecified files
	for (size_t i = 0; i < tempHeaderFilesList.size(); i++)
	{
		if (tempHeaderFilesList[i] != nullptr)
		{
			headerFiles.push_back(tempHeaderFilesList[i]);
			tempHeaderFilesList[i] = nullptr;
		}
	}

	headersAreSorted = true;
}

void FileSorter::CopyFileHeapToVector(FileHeap* _from, std::vector<File*>& _to)
{
	_to.clear();
	for (auto iter = _from->begin(); iter != _from->end(); ++iter)
		_to.push_back(iter->second);
}

int FileSorter::GetFileIndexByName(std::string _fileName, std::vector<File*> _fileList)
{
	for (int i = 0; i < _fileList.size(); i++)
	{
		if (_fileList[i] != nullptr)
			if (_fileList[i]->GetName().compare(_fileName) == 0)
				return i;
	}
	return -1;
}