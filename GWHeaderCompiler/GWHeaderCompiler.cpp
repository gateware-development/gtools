#include "pch.h"
#include "GWHeaderCompiler.h"
#include "GWException.h"
#include "PredefinedRegex.h"
#include <iostream>
#include <assert.h>

GWHeaderCompiler::GWHeaderCompiler(CommandLineArgumentHandler& _handler)
{
	interfaceDirectoryPath = _handler.GetInterfacePath();
	outputDirectoryPath = _handler.GetOutputPath();
	licensePath = _handler.GetLicensePath();
	headerListPath = _handler.GetHeaderListPath();
	documentationRemover = DocumentationRemover(outputDirectoryPath);

	CopyStringVector(_handler.GetPrependFilePaths(), prependFilePaths);
	CopyStringVector(_handler.GetAppendFilePaths(), appendFilePaths);

	std::cout << "GWHeaderCompiler Version: " + std::to_string(VERSION_MAJOR) + "." + std::to_string(VERSION_MINOR) + "." + std::to_string(VERSION_PATCH) << std::endl;
	ThrowIfFailed(gFile.Create());

	InitializeGLog();
	readWriteHandler = ReadWriteHandler(outputDirectoryPath, &gLog);

	std::string path, name;
	size_t index = interfaceDirectoryPath.find_last_of("/");
	// there is a path
	if (index != std::string::npos)
	{
		path = interfaceDirectoryPath.substr(0, index);
		name = interfaceDirectoryPath.substr(index + 1);
	}
	// no path
	else
	{
		path = "./";
		name = interfaceDirectoryPath;
	}

	Directory* rootDirectory = new Directory(path, name, 0);
	LoadDirectory(rootDirectory);
	LoadIncludeHierarchy(rootDirectory);
	delete rootDirectory;
}

void GWHeaderCompiler::CopyStringVector(const std::vector<std::string> _from, std::vector<std::string>& _to)
{
	for (size_t i = 0; i < _from.size(); i++)
	{
		_to.push_back(_from[i]);
	}
}

void GWHeaderCompiler::InitializeGLog()
{
	ThrowIfFailed(gLog.Create("Output.txt"));
	ThrowIfFailed(gLog.EnableConsoleLogging(true));
	ThrowIfFailed(gLog.EnableVerboseLogging(false));
}

void GWHeaderCompiler::LoadDirectory(Directory* rootDirectory)
{
	if (rootDirectory)
	{
		LogMessage("Root Directory: " + rootDirectory->GetFullPath());
		for (unsigned int i = 0; i < rootDirectory->GetSubdirectories().size(); ++i)
		{
			ProcessDirectory(rootDirectory->GetSubdirectories()[i], 0);
			LogMessage();
		}
	}
}
void GWHeaderCompiler::LoadIncludeHierarchy(Directory* rootDirectory)
{
	if (rootDirectory)
	{
		LogMessage("\nRAW Include Hierarchy View:\nRoot Directory: " + rootDirectory->GetFullPath());
		for (unsigned int i = 0; i < rootDirectory->GetSubdirectories().size(); ++i)
		{
			ProcessIncludeHierarchy(rootDirectory->GetSubdirectories()[i], 0);
			LogMessage();
		}
	}
}

void GWHeaderCompiler::ProcessDirectory(Directory* directory, unsigned int depth)
{
	if (directory)
	{
		// Pure data processing and outputs
		std::string logMsg;
		for (unsigned int i = 0; i < depth; ++i)
		{
			logMsg += "|     ";
		}
		logMsg += "+-Directory: ";
		logMsg += directory->GetName().data();
		logMsg += ", File count: " + std::to_string(directory->GetFiles().size());
		LogMessage(logMsg);
		for (unsigned int i = 0; i < directory->GetFiles().size(); ++i)
		{
			logMsg.clear();
			for (unsigned int i = 0; i < depth; ++i)
			{
				logMsg += "|     ";
			}
			logMsg += "|\t+-";
			auto currentFile = directory->GetFiles()[i];
			logMsg += "File[" + std::to_string(i) + "]: " + currentFile->GetName().data();
			logMsg += ", File Size: " + std::to_string(currentFile->GetByteSize()) + " Bytes";
			LogMessage(logMsg);
		}
		// Recursion
		for (unsigned int i = 0; i < directory->GetSubdirectories().size(); ++i)
		{
			ProcessDirectory(directory->GetSubdirectories()[i], depth + 1);
		}
	}
}
void GWHeaderCompiler::ProcessIncludeHierarchy(Directory* directory, unsigned int depth)
{
	if (directory)
	{
		std::string logMsg;
		for (unsigned int i = 0; i < depth; ++i)
		{
			logMsg += "|     ";
		}
		logMsg += "+-Directory: ";
		logMsg += directory->GetName();
		LogMessage(logMsg);
		for (unsigned int i = 0; i < directory->GetFiles().size(); ++i)
		{
			logMsg.clear();
			for (unsigned int i = 0; i < depth; ++i)
			{
				logMsg += "|     ";
			}
			logMsg += "|\t+-File: ";
			auto currentFile = directory->GetFiles()[i];
			// This is where we gather all the files to our map (associative containers, MAP), I could've used unordered_map, however I need the names sorted
			if (currentFile->IsH())
			{
				headerHeap.Create(currentFile->GetPath(), currentFile->GetName());
			}
			else if (currentFile->IsHPP())
			{
				sourceHeap.Create(currentFile->GetPath(), currentFile->GetName());
			}

			logMsg += currentFile->GetName().data();
			LogMessage(logMsg);
		}
		for (unsigned int i = 0; i < directory->GetSubdirectories().size(); ++i)
		{
			ProcessIncludeHierarchy(directory->GetSubdirectories()[i], depth + 1);
		}
	}
}

void GWHeaderCompiler::LogMessage(const std::string& message)
{
	gLog.Log(message.data());
	gLog.Flush();
}
void GWHeaderCompiler::LogNewline()
{
	LogMessage();
}


void GWHeaderCompiler::Build()
{
	PreprocessAllFiles();
	fileSorter = FileSorter(&headerHeap);
	fileSorter.SetHeaderListPath(headerListPath);
	fileSorter.SortHeaders();

	readWriteHandler.OpenWrite();
	readWriteHandler.WriteBeginningOfIncludeGuard();
	readWriteHandler.WriteCompilerVersionInfo();
	readWriteHandler.WriteFileByPathAsComment(licensePath);
	readWriteHandler.WriteFilesByPath(prependFilePaths);


	readWriteHandler.WriteFiles(fileSorter.GetSortedHeaderList());
	readWriteHandler.WriteFilesByPath(appendFilePaths);
	readWriteHandler.WriteEndOfIncludeGuard();
	readWriteHandler.FlushFile();
	readWriteHandler.CloseWrite();

	documentationRemover.RemoveDoxygen();


	LogNewline();
	LogMessage("Single header generation complete!");
}

void GWHeaderCompiler::PreprocessAllFiles()
{
	for (auto iter = headerHeap.begin(); iter != headerHeap.end(); ++iter)
		iter->second->Preprocess(headerHeap, sourceHeap);
}