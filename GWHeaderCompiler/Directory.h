#ifndef _DIRECTORY_H_
#define _DIRECTORY_H_
#include "File.h"

class Directory
{
private:
	unsigned int depth; // 0 if is root else depth + 1
	bool isRoot; // if this directory is the root dir
	std::string rootFullPath; // only used if isRoot is true
	std::string path; // absolute path
	std::string name;
	std::string fullPath; // path + name
	std::vector<File*> files;
	std::vector<Directory*> subdirectories;

	GW::SYSTEM::GFile fileHandler;
public:
	Directory(const std::string& _path, const std::string& _name, unsigned int _depth);
	~Directory();

	unsigned int GetDepth() const;
	const std::string& GetPath() const;
	const std::string& GetName() const;
	const std::string& GetFullPath() const;
	const std::vector<File*>& GetFiles() const;
	const std::vector<Directory*>& GetSubdirectories() const;
};
#endif