#include "pch.h"
#if defined(_DEBUG)
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <cstdlib>
#include <crtdbg.h>
#endif
#include <iostream>
#include "GWHeaderCompiler.h"
#include "GWException.h"

int main(int argc, char** argv)
{
#if !defined NDEBUG && defined _WIN64
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
	_CrtSetBreakAlloc(-1); // BREAKPOINT
#endif

	auto waitforinput = []()
	{
#ifndef NDEBUG
		std::cout << "\npress any key to continue";
		std::cin.get();
#endif
	};

	try
	{
		if ((argc - 1) == 0)
			throw std::runtime_error("Command line arguments should not be empty!");

		CommandLineArgumentHandler argumentHandler = CommandLineArgumentHandler(argc, argv);

		if (argumentHandler.HelpWasRequested())
			argumentHandler.PrintHelpInfo();
		else if (argumentHandler.ShouldGenerate())
		{
			GWHeaderCompiler gwheadercompiler(argumentHandler);

			gwheadercompiler.Build();
		}
		else
			throw std::runtime_error("unknown options or bad parameter\navailable options:\n\t[--help]\n\t[--generate]");

#if !defined NDEBUG && defined _WIN64
		_CrtCheckMemory(); //memory leak detection 
#endif

		waitforinput();
		return 0;
	}
	catch (GWException& gwe)
	{
		std::cout << gwe.ToString() << std::endl;
		waitforinput();
		return 1;
	}
	catch (std::runtime_error& stdre)
	{
		std::cout << stdre.what() << std::endl;
	}
}