#include "pch.h"
#include "GWException.h"
using namespace GW;

GWException::GWException(GReturn code, const std::string& functionName, const std::string& fileName, int lineNumber)
	:
	Code(code), FunctionName(functionName), FileName(fileName), LineNumber(lineNumber)
{}

std::string GWException::ToString() const
{
	std::string msg;
	msg = FunctionName + " failed at line: ";
	msg += std::to_string(LineNumber);
	msg += "; Failed in file: " + FileName;
	switch (Code)
	{
	case GReturn::FAILURE:
	{
		msg += "; Error: GReturn::FAILURE";
	}
	break;
	case GReturn::FEATURE_UNSUPPORTED:
	{
		msg += "; Error: GReturn::FEATURE_UNSUPPORTED";
	}
	break;
	case GReturn::FILE_NOT_FOUND:
	{
		msg += "; Error: GReturn::FILE_NOT_FOUND";
	}
	break;
	case GReturn::INTERFACE_UNSUPPORTED:
	{
		msg += "; Error: GReturn::INTERFACE_UNSUPPORTED";
	}
	break;
	case GReturn::INVALID_ARGUMENT:
	{
		msg += "; Error: GReturn::INVALID_ARGUMENT";
	}
	break;
	case GReturn::MEMORY_CORRUPTION:
	{
		msg += "; Error: GReturn::MEMORY_CORRUPTION";
	}
	break;
	case GReturn::REDUNDANT:
	{
		msg += "; Error: GReturn::REDUNDANT_OPERATION";
	}
	break;
	}
	return msg;
}
