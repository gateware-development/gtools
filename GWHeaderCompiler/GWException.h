#ifndef _GWEXCEPTION_H_
#define _GWEXCEPTION_H_
#include "../Gateware-Development/Interface/Core/GCoreDefines.h"
#include <string>

class GWException
{
private:
	GW::GReturn Code = GW::GReturn::SUCCESS;
	std::string FunctionName;
	std::string FileName;
	int LineNumber = -1;
public:
	GWException(GW::GReturn code, const std::string& functionName, const std::string& fileName, int lineNumber);
	std::string ToString() const;
};

#ifndef ThrowIfFailed
#define ThrowIfFailed(x)												\
{																		\
	GW::GReturn gr = (x);												\
	if (G_FAIL(gr)){ throw GWException(gr, #x, __FILE__, __LINE__); }	\
}
#endif
#endif
