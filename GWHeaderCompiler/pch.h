#ifndef _PCH_H_
#define _PCH_H_
#define GATEWARE_ENABLE_CORE
#define GATEWARE_ENABLE_SYSTEM
#include "../Gateware-Development/Interface/System/GFile.h"
#include "../Gateware-Development/Interface/System/GLog.h"
#include <vector>

#define VERSION_MAJOR 1 
#define VERSION_MINOR 5 
#define VERSION_PATCH 1 

#if defined (_WIN64)
//Platform-appropriate file delimiter ("\\" for windows and "/" for mac and linux)
#define FILE_DELIMITER (std::string)"\\"
#else
//Platform-appropriate file delimiter ("\\" for windows and "/" for mac and linux)
#define FILE_DELIMITER (std::string)"/"
#endif
//the string cast is sadly needed to allow the delimiter to be insterted into the middle of strings. (You can't concatenate a string to a char) 
#endif