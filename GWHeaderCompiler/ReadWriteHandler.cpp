#include "ReadWriteHandler.h"
#include "GWException.h"
#include <assert.h>

ReadWriteHandler::ReadWriteHandler(std::string _outputPath, GW::SYSTEM::GLog* _log)
{
	ThrowIfFailed(readHandler.Create());
	ThrowIfFailed(writeHandler.Create());
	ThrowIfFailed(writeHandler.SetCurrentWorkingDirectory(_outputPath.data()));
	log = _log;
}

void ReadWriteHandler::OpenWrite()
{
	ThrowIfFailed(writeHandler.OpenBinaryWrite("Gateware.h"));
}
void ReadWriteHandler::CloseWrite()
{
	writeHandler.CloseFile();
}

void ReadWriteHandler::WriteNewline()
{
	ThrowIfFailed(writeHandler.Write("\n\r", 2));
}

void ReadWriteHandler::WriteFile(const File* _toWrite)
{
	if (_toWrite->IsExternalFile() == false)
	{
		ThrowIfFailed(writeHandler.Write(_toWrite->GetRawData().data(), _toWrite->GetByteSize()));
		WriteNewline();
		LogMessage("Wrote file: " + _toWrite->GetName() + "\n");
	}
}
void ReadWriteHandler::WriteFiles(const std::vector<File*>* _filesToWrite)
{
	for (size_t i = 0; i < _filesToWrite->size(); i++)
		WriteFile((*_filesToWrite)[i]);
}
void ReadWriteHandler::WriteString(const std::string _toWrite)
{
	assert(_toWrite.size() > 0 && "ReadWriteHandler::WriteString: Received empty or invalid string!");
	ThrowIfFailed(writeHandler.Write(_toWrite.data(), static_cast<unsigned int>(_toWrite.size())));
	WriteNewline();
}
void ReadWriteHandler::WriteFileByPath(std::string _path)
{
	std::string fileData;
	ReadFileIntoString(_path, fileData);
	WriteString(fileData);
}
void ReadWriteHandler::WriteFilesByPath(std::vector<std::string>& _filePathsToWrite)
{
	for (size_t i = 0; i < _filePathsToWrite.size(); i++)
		WriteFileByPath(_filePathsToWrite[i]);
}

void ReadWriteHandler::ReadFileIntoString(const std::string _filePath, std::string& _outString)
{
	std::string fileName = _filePath.substr(_filePath.find_last_of("/") + 1);
	std::string relativePath = _filePath.substr(0, _filePath.find_last_of("/"));

	readHandler.SetCurrentWorkingDirectory(relativePath.data());
	unsigned int fileSize = 0;
	ThrowIfFailed(readHandler.GetFileSize(fileName.data(), fileSize));
	ThrowIfFailed(readHandler.OpenBinaryRead(fileName.data()));
	_outString.resize(fileSize);
	ThrowIfFailed(readHandler.Read(const_cast<char*>(_outString.data()), fileSize));
	ThrowIfFailed(readHandler.CloseFile());
}

void ReadWriteHandler::FlushFile()
{
	writeHandler.FlushFile();
}

void ReadWriteHandler::WriteBeginMarker(const std::string& _name)
{
	std::string beginMarker;
	beginMarker = "\n/*---------------------------------\n";
	beginMarker += "|\tBegin of " + _name;
	beginMarker += "\n----------------------------------*/\n";
	WriteString(beginMarker);
}
void ReadWriteHandler::WriteEndMarker(const std::string& _name)
{
	std::string endMarker;
	endMarker = "\n/*---------------------------------\n";
	endMarker += "|\tEnd of " + _name;
	endMarker += "\n----------------------------------*/\n\n";
	WriteString(endMarker);
}

void ReadWriteHandler::WriteBeginningOfIncludeGuard()
{
	std::string includeGuardBegin = "#ifndef GATEWARE_H\n#define GATEWARE_H\n\n";
	WriteString(includeGuardBegin);
}
void ReadWriteHandler::WriteEndOfIncludeGuard()
{
	std::string includeGuardEnd = "\n\n#endif // End of GATEWARE_H";
	WriteString(includeGuardEnd);
}
void ReadWriteHandler::WriteCompilerVersionInfo()
{
	std::string versionInfo = "/* File created by GW Header Compiler version ";
	versionInfo += std::to_string(VERSION_MAJOR) + "." + std::to_string(VERSION_MINOR) + "." + std::to_string(VERSION_PATCH) + " */\n\n";
	WriteString(versionInfo);
}
void ReadWriteHandler::WriteFileByPathAsComment(std::string _filePath)
{
	std::string fileData;
	ReadFileIntoString(_filePath, fileData);
	fileData = "/*\n" + fileData + "\n*/\n"; //comment out the contents of the file
	WriteString(fileData);
}

void ReadWriteHandler::LogMessage(std::string _msg)
{
	log->Log(_msg.data());
	log->Flush();
}