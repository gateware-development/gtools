#ifndef _GWHEADERCOMPILER_H_
#define _GWHEADERCOMPILER_H_
#define GATEWARE_ENABLE_CORE
#define GATEWARE_ENABLE_SYSTEM
#include "../Gateware-Development/Interface/System/GLog.h"
#include "Directory.h"
#include "FileHeap.h"
#include "CommandLineArgumentHandler.h"
#include "ReadWriteHandler.h"
#include "DocumentationRemover.h"
#include "FileSorter.h"

class GWHeaderCompiler
{
private:
	GW::SYSTEM::GFile gFile;
	GW::SYSTEM::GLog gLog;
	ReadWriteHandler readWriteHandler;
	DocumentationRemover documentationRemover;
	FileSorter fileSorter;

	FileHeap headerHeap;
	FileHeap sourceHeap;


	std::string interfaceDirectoryPath, outputDirectoryPath, licensePath, headerListPath;
	std::vector<std::string> prependFilePaths, appendFilePaths;

	std::vector<File*> headerFiles;
	std::unordered_map<File*, bool> includedFiles;

	void CopyStringVector(const std::vector<std::string> from, std::vector<std::string>& to);
	
	void InitializeGLog();

	void LoadDirectory(Directory* rootDirectory); // Initiating function
	void LoadIncludeHierarchy(Directory* rootDirectory); // Initiating function

	void ProcessDirectory(Directory* directory, unsigned int depth); // Recursive function
	void ProcessIncludeHierarchy(Directory* directory, unsigned int depth); // Recursive function

	// Automatically appends new line at the end
	void LogMessage(const std::string& message = "");
	void LogNewline();

	void PreprocessAllFiles();

public:
	GWHeaderCompiler(CommandLineArgumentHandler& _handler);
	~GWHeaderCompiler() = default;

	void Build();
};
#endif