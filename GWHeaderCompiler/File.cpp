#include "File.h"
#include "GWException.h"
#include "PredefinedRegex.h"
#include "FileHeap.h"
#include <assert.h>

std::string File::GetFileExtension(const std::string& name) const
{
	return name.substr(name.find_last_of(".") + 1);
}

File::File(const std::string& _path, const std::string& _name, GW::SYSTEM::GFile* _fileHandler)
	: path(_path), name(_name), isExternalFile(false)
{
	assert(_fileHandler != nullptr && "File requires a valid file handler to operate!");
	fileHandler = _fileHandler;

	DetermineFileType();
	if(type == FileType::eOther)
		return;


	fullPath = path + FILE_DELIMITER + name;

	ThrowIfFailed(fileHandler->SetCurrentWorkingDirectory(path.data()));

	unsigned int fileSize;
	if (+fileHandler->GetFileSize(name.data(), fileSize))
	{
		if (fileSize > 0)
			ReadDataFromFile(fileSize);
	}
	else
	{
		isExternalFile = true;
		WarnThatWeAreAnExternalFile();
	}
}

void File::WarnThatWeAreAnExternalFile()
{
	GW::SYSTEM::GLog log;
	log.Create("Warning.txt");
	log.EnableConsoleLogging(true);
	log.EnableVerboseLogging(false);
	std::string msg = "File is not found: " + name + "\tThis file might be an external library file that is not part of Gateware!";
	log.Log(msg.data());
	log.Flush();
}

void File::ReadDataFromFile(unsigned int& fileSize)
{
	rawData.resize(fileSize);
	fileHandler->OpenBinaryRead(name.data());
	fileHandler->Read(const_cast<char*>(rawData.data()), fileSize);
	fileHandler->CloseFile();
	rawData += '\n';
}

void File::DetermineFileType()
{
	std::string ext = GetFileExtension(name);
	if (ext == "h")
		type = FileType::eH;
	else if (ext == "hpp")
		type = FileType::eHPP;
	else
		type = FileType::eOther;
}


void File::Preprocess(FileHeap& headerHeap, FileHeap& sourceHeap)
{
	assert(fileHandler != nullptr && "File requires a valid file handler to operate!");
	// iterate through the raw data and read line by line until we found an include
	for (auto iter = std::sregex_iterator(rawData.begin(), rawData.end(), REGEX::PoundInclude); iter != std::sregex_iterator(); ++iter)
	{
		std::smatch match = *iter;
		std::string lineString = match.str(0);
		std::string includeDirectory, includeFileName;
		size_t lastDelimiterIndex = lineString.find_last_of("/");
		// there is a file directory
		if (lastDelimiterIndex != std::string::npos)
		{
			size_t firstQuoteMarkIndex = lineString.find_first_of("\"");
			includeDirectory = path + FILE_DELIMITER + lineString.substr(firstQuoteMarkIndex + 1, lastDelimiterIndex - firstQuoteMarkIndex - 1);
			includeFileName = lineString.substr(lastDelimiterIndex + 1, lineString.find_last_of("\"") - lastDelimiterIndex - 1);

			std::string temp = GetAbsolutePath(includeDirectory); 
			includeDirectory = temp.substr(0, temp.find_last_of(FILE_DELIMITER));

		}
		// the file directory is in the same directory as this file
		else
		{
			lastDelimiterIndex = lineString.find_first_of("\"");
			includeDirectory = path;
			includeFileName = lineString.substr(lastDelimiterIndex + 1, lineString.find_last_of("\"") - lastDelimiterIndex - 1);
		}
		// reset the working directory back to this files path
		fileHandler->SetCurrentWorkingDirectory(path.data());
		std::string ext = GetFileExtension(includeFileName);

		if (ext == "h")
			AddFileToHeapAndVector(includeDirectory, includeFileName, headerHeap, includeFiles);
		else if (ext == "hpp")
			AddFileToHeapAndVector(includeDirectory, includeFileName, sourceHeap, includeFiles);
	}

	for (size_t i = 0; i < includeFiles.size(); ++i)
	{
		includeFiles[i]->Preprocess(headerHeap, sourceHeap);
	}

	std::string newRawData = rawData; // i need a place holder for rawData is because regex_iter will become invalid if original content were modified
	for (auto iter = std::sregex_iterator(rawData.begin(), rawData.end(), REGEX::PoundInclude); iter != std::sregex_iterator(); ++iter)
	{
		std::smatch match = *iter;
		std::string lineString = match.str(0);
		std::string truncatedString = RetrieveFileName(lineString);

		std::regex regex(lineString);
		File* headerFile = headerHeap.Find(truncatedString);
		if (headerFile)
		{
			if (headerFile->isExternalFile)
			{
				std::string substitution = lineString + "\t// This file might be an external library file that is not part of Gateware!";
				newRawData = std::regex_replace(newRawData, regex, substitution);
				continue;
			}
			else
			{
				//if (this->path == headerFile->path)
				//	++headerFile->dependencyCount;
				std::string substitution = "";
				newRawData = std::regex_replace(newRawData, regex, substitution);
				continue;
			}
		}

		auto sourceFile = sourceHeap.Find(truncatedString);
		if (sourceFile)
			newRawData = std::regex_replace(newRawData, regex, sourceFile->GetRawData());
	}
	rawData = std::move(newRawData);
}

std::string File::RetrieveFileName(const std::string _fileName)
{
	std::string truncatedName;
	size_t index = _fileName.find_last_of("/");
	// there is a file directory, if thats the case, we just want the name of the file and remove the last "
	if (index != std::string::npos)
	{
		truncatedName = _fileName.substr(_fileName.find_last_of("/") + 1);
		truncatedName.erase(truncatedName.size() - 1);
	}
	else // there is no directory for this file, remove the #include and ""
	{
		truncatedName = _fileName.substr(_fileName.find_first_of("\"") + 1);
		truncatedName.erase(truncatedName.size() - 1);
	}
	return truncatedName;
}

void File::AddFileToHeapAndVector(const std::string _path, const std::string _name, FileHeap& _heap, std::vector<File*>& _fileVector)
{
	File* file = _heap.Find(_name);
	if(file == nullptr)
		_fileVector.push_back(_heap.Create(_path, _name));
	else
		_fileVector.push_back(file);
}

std::string File::GetAbsolutePath(std::string _relativePath)
{
	std::string retval;
	retval.resize(260);
	fileHandler->SetCurrentWorkingDirectory(_relativePath.data());
	fileHandler->GetCurrentWorkingDirectory(const_cast<char*>(retval.data()), 260);
	return retval;
}

//unsigned int File::GetDependencyCount() const { return dependencyCount; }
const std::string& File::GetPath() const { return path; }
const std::string& File::GetName() const { return name; }
const std::string& File::GetFullPath() const { return fullPath; }
unsigned int File::GetByteSize() const { return static_cast<unsigned int>(rawData.size()); }
const std::string& File::GetRawData() const { return rawData; }

const std::vector<File*>& File::GetIncludeFiles() const { return includeFiles; }

bool File::IsH() const { return type == FileType::eH; }
bool File::IsHPP() const { return type == FileType::eHPP; }
bool File::IsExternalFile() const { return isExternalFile; }