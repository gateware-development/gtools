@echo off
SETLOCAL EnableDelayedExpansion
SETLOCAL

::switches
set /a PrintHelp = 0
set /a NoClone = 0
set /a Overwrite = 1
set /a UpdateVersionNumber = 1

::loop through arguments and set switches accordingly
for %%x in (%*) do (
	if /i "%%x" == "--Help" set /a PrintHelp = 1
	if /i "%%x" == "--NoClone" set /a NoClone = 1
	if /i "%%x" == "--NoOverwrite" set /a Overwrite = 0
	if /i "%%x" == "--DoNotUpdateVersionNumber" set /a UpdateVersionNumber = 0
)

::Print help
if !PrintHelp! == 1 (
	echo This script is built to automatically generate Gateware's documentation
	echo Arguments:
	echo	--Help						
	echo		Prints this help text
	echo	--NoClone					
	echo		Specifies that the gsource repo does not need to be cloned - this script will assume it's already been cloned
	echo	--NoOverwrite				
	echo		Specifies that this script should not delete and replace the Documentation folder if it already exists
	echo	--DoNotUpdateVersionNumber	
	echo		Prevents this script from generating a fresh version number for the documentation
	EXIT /B
)


::Clone repo
if !NoClone! == 1 (
if not exist ../../Gateware-Development (
	echo Development repo cannot be found, this script cannot run without one!
	EXIT /B 2 
	)
) else (
	call ShallowCloneDevelopmentRepo.bat
)


::Remove existing docs
if !Overwrite! == 1 (
	cd ../../
	if exist Documentation RMDIR Documentation /q /s
	cd scripts/windows
)


::Update version number
if !UpdateVersionNumber! == 1 (
	cd ../../Gateware-Development/DevOps
	cmake -P ./GenVerHpp.cmake
	cd ../../scripts/windows
)

::append a version number to the doxyfile
cd ../../Gateware-Development/Deploy
set versionNumber="INVALID VERSION"
for /F %%L in (version.txt) do (::apparently a for loop is needed to read a plaintext file even if it's only one line?
	set versionNumber=%%L
)
cd ../../
cd Gateware-Development/DevOps/Doxygen
echo PROJECT_NUMBER=!versionNumber!>>doxyfile
cd ../../../scripts/windows

::generate the documentation
cd ../../Gateware-Development/DevOps/Doxygen
doxygen
cd ../../../

::move the documentation to the root of the repo
move /y Gateware-Development/Documentation ./

ENDLOCAL