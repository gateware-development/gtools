git clone https://gitlab.com/gateware-development/gsource.git ../../Gateware-Development --depth=1 --branch 192-bringing-uwp-branch-up-to-speed --single-branch

cd ../../Gateware-Development
if(-not(Test-Path build)) {mkdir build}
cd build

if(-not(Test-Path UWP)) {mkdir UWP}
cd UWP
cmake -A x64 "../../../UWP/"  #make all the projects
cd ../ 

if(-not(Test-Path Desktop)) {mkdir Desktop}
cd Desktop
cmake -A x64 "../../../"  #make all the projects 



cmake --build "./" --target "SingleHeaderCompiler" --config "Release" #build the SingleHeaderCompiler executable
mv ../Desktop/GWHeaderCompiler/Release/SingleHeaderCompiler.exe ../ -force #move the executable to root build folder 

cd ../../../
cd Gateware-Development/DevOps
cmake -P ./GenVerHpp.cmake 

cd ../../
cd Scripts
cmake -P ./GenSingleHeader.cmake
cd ../

cmake --build "./Gateware-Development/build/UWP" --target "SingleHeaderTestMain" --config "Debug"
cmake --build "./Gateware-Development/build/UWP" --target "SingleHeaderTestMain" --config "Release"

pause
