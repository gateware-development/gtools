#!/bin/bash

if [! -d "../../Gateware-Development"]; then
git clone https://gitlab.com/gateware-development/gsource.git ../../Gateware-Development --depth=1 --branch master --single-branch
fi

cd ../../Gateware-Development
mkdir -p build
cd build

cmake ./../../ -DCMAKE_OSX_ARCHITECTURES="x86_64" -G "Xcode" 