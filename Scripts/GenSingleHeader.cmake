########################################################################
# This Cmake script generates a new Gateware.h each time you compile

message("Updating Single Header from Source...")

if(WIN32)
	set(HEADER_COMPILER_BINARY_NAME SingleHeaderCompiler.exe)
else()
	set(HEADER_COMPILER_BINARY_NAME SingleHeaderCompiler)
endif()

find_file(HEADER_COMPILER_LOCATION NAMES ${HEADER_COMPILER_BINARY_NAME} PATHS "${CMAKE_CURRENT_LIST_DIR}/../Gateware-Development/build" NO_DEFAULT_PATH)

message(STATUS "Path to compiler binary:")
message("${HEADER_COMPILER_LOCATION}")

if (${HEADER_COMPILER_LOCATION} STRLESS_EQUAL "HEADER_COMPILER_LOCATION-NOTFOUND")

	execute_process(
		COMMAND ${HEADER_COMPILER_LOCATION} --generate --interface ./../Interface --license ./../LICENSE --prepend  ./../Source/Shared/GVersion.hpp ./../Source/Shared/WarningSuppressions.hpp --append ./../Source/Shared/GDependenciesTest.hpp ./../Source/Shared/WarningUnsuppressions.hpp --output ./ --header-list ./../DevOps/HeaderList.txt
		WORKING_DIRECTORY "${CMAKE_CURRENT_LIST_DIR}/../Gateware-Development/build"
		RESULT_VARIABLE HEADER_COMPILER_RESULT	
	)

	if(${HEADER_COMPILER_RESULT} EQUAL 0)
		message("Single Header generation complete.")
	else()
		message(WARNING ${HEADER_COMPILER_RESULT})
		message(FATAL_ERROR "Single Header generation has failed:")
	endif()

else()
	message(WARNING "No SH compiler binary found, header creation failed...")
endif()
########################################################################