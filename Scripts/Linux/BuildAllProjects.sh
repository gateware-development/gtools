#!/bin/bash

if [! -d "../../Gateware-Development"]; then
	git clone https://gitlab.com/gateware-development/gsource.git ../Gateware-Development --depth=1 --branch master --single-branch
fi

cd ../../Gateware-Development
mkdir -p build
cd build

mkdir -p debug
cd debug
cmake ./../../../ -G "CodeLite - Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug;
rm debug
cd ../

mkdir -p release
cd release
touch release
cmake ./../../../ -G "CodeLite - Unix Makefiles" -DCMAKE_BUILD_TYPE=Release;